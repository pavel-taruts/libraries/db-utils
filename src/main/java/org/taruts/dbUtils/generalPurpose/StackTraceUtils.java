package org.taruts.dbUtils.generalPurpose;

public class StackTraceUtils {

    private static StackTraceElement getStackTraceElement(int callerLevel) {

        StackTraceElement[] ste = Thread.currentThread().getStackTrace();

        return ste[1 /*пропускаем Thread.getStackTrace*/ + 1 /*пропускаем текущий метод*/ + callerLevel];
    }

    public static Class<?> getCallerClass(int callerLevel) {

        StackTraceElement ste = getStackTraceElement(1 + callerLevel);

        String className = ste.getClassName();

        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
