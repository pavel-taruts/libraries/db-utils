package org.taruts.dbUtils.generalPurpose;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;

public class ResourceUtils {

    private static final Logger logger = LoggerFactory.getLogger(ResourceUtils.class);

    public static String getResourceAsString(Class<?> clazz, String name, String encoding) {
        try {
            URL resourceUrl = clazz.getResource(name);
            if (resourceUrl == null) {
                throw new NullPointerException();
            }
            return IOUtils.toString(resourceUrl, encoding);
        } catch (Exception e) {
            logger.error("Не получилось получить ресурс " + name + " класса " + clazz.getSimpleName());
            throw new RuntimeException(e);
        }
    }

    public static String getSql(Class<?> clazz, String name) {
        return getResourceAsString(clazz, name, "UTF-8");
    }

    /**
     * @param name Имя запроса. Является частью имени файла .sql, без имени класса вначале, и без .sql в конце
     * @return Содержимое файла &lt;Имя класса&gt;.&lt;значение параметра name&gt;.sql
     */
    public static String getSql(String name) {

        // Получаем класс того, кто вызвал
        Class<?> clazz = StackTraceUtils.getCallerClass(1);

        return getSql(clazz, name);
    }
}
