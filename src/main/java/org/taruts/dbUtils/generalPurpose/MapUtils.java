package org.taruts.dbUtils.generalPurpose;

import java.util.HashMap;
import java.util.Map;

public class MapUtils {

    public static <T> Map<String, T> asPropertyMap(Object... parts) {
        return asMap(parts);
    }

    public static <K, V> Map<K, V> asMap(Object... parts) {

        if (parts.length % 2 != 0) {
            throw new RuntimeException("the array parameter's size must not be an odd number");
        }

        int mapSize = parts.length / 2;

        Map<K, V> map = new HashMap<>(mapSize);

        for (int i = 0; i < mapSize; i++) {

            @SuppressWarnings("unchecked")
            K key = (K) parts[2 * i];

            @SuppressWarnings("unchecked")
            V value = (V) parts[2 * i + 1];

            map.put(key, value);
        }

        return map;
    }
}
