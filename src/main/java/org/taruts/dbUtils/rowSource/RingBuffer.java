package org.taruts.dbUtils.rowSource;

import com.google.common.base.Preconditions;

@SuppressWarnings("unused")
public class RingBuffer<T> {

    final private int maxBackwardSize;
    final private int maxForwardSize;
    final private int maxSize;

    final private T[] data;

    private int currentIndex;

    private int backwardSize;
    private int forwardSize;

    private boolean hasCurrent;

    public RingBuffer(int maxBackwardSize, int maxForwardSize) {

        this.maxBackwardSize = maxBackwardSize;
        this.maxForwardSize = maxForwardSize;
        this.maxSize = maxBackwardSize + maxForwardSize + 1;

        //noinspection unchecked
        this.data = (T[]) new Object[maxSize];

        this.currentIndex = maxSize - 1;
        this.backwardSize = 0;
        this.forwardSize = 0;
        this.hasCurrent = false;
    }

    public void setNext(int i, T t) {
        int index = getNextIndex(i);
        data[index] = t;
    }

    public void setPrevious(int i, T t) {
        int index = getPreviousIndex(i);
        data[index] = t;
    }

    public void append(T t) {

        Preconditions.checkState(forwardSize < maxForwardSize, "forwardSize has already reached maxForwardSize, that is %s", maxForwardSize);

        forwardSize++;

        setNext(forwardSize - 1, t);
    }

    public void shiftForward() {

        Preconditions.checkState(forwardSize > 0, "forwardSize is zero, there's no place to move the current position at");

        forwardSize--;

        // Смотрим, не накопилось ли предыдущих слишком много
        if (backwardSize == maxBackwardSize) {
            // Накопилось. Вместо последнего ставим null
            setPrevious(maxBackwardSize - 1, null);
        } else {
            if (hasCurrent) {
                backwardSize++;
            }
        }

        hasCurrent = true;

        currentIndex = (currentIndex + 1) % maxSize;
    }

    public int getForwardSize() {
        return forwardSize;
    }

    public boolean hasNext() {
        return forwardSize > 0;
    }

    public boolean hasCurrent() {
        return hasCurrent;
    }

    public boolean hasPrevious() {
        return backwardSize > 0;
    }

    public int getMaxBackwardSize() {
        return maxBackwardSize;
    }

    public int getMaxForwardSize() {
        return maxForwardSize;
    }

    public T getCurrent() {

        Preconditions.checkState(hasCurrent, "can't get the current element, there's no one");

        return data[currentIndex];
    }

    public void setCurrent(T t) {
        hasCurrent = true;
        data[currentIndex] = t;
    }

    public T getNext(int i) {

        int index = getNextIndex(i);

        return data[index];
    }

    private int getNextIndex(int i) {
        Preconditions.checkArgument(hasNext(i), "There's no next element with index %s", i);

        return (currentIndex + 1 + i) % maxSize;
    }

    public boolean hasNext(int i) {
        return i < forwardSize;
    }

    public T getPrevious(int i) {
        int index = getPreviousIndex(i);
        return data[index];
    }

    private int getPreviousIndex(int i) {
        Preconditions.checkArgument(hasPrevious(i), "There's no previous element with index %s", i);
        return (currentIndex - 1 - i + maxSize) % maxSize;
    }

    public boolean hasPrevious(int i) {
        return i < backwardSize;
    }

    public int getBackwardSize() {
        return backwardSize;
    }
}
