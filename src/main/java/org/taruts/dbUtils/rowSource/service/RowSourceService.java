package org.taruts.dbUtils.rowSource.service;

import com.google.common.base.Joiner;
import org.hibernate.QueryParameterException;
import org.hibernate.SQLQuery;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.taruts.dbUtils.generalPurpose.ResourceUtils;
import org.taruts.dbUtils.jdbc.JdbcUtils;
import org.taruts.dbUtils.rowSource.RowSource;
import org.taruts.dbUtils.rowSource.RowSourceParameters;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RowSourceService {

    private static final Logger logger = LoggerFactory.getLogger(RowSourceService.class);

    private static final Pattern TEMP_TABLE_PATTERN = Pattern.compile("(?i)create\\s+(?:|(temp|temporary)\\s+)table\\s+(?:(?!if)|if\\s+not\\s+exists\\s+)([\\w|.]+)");

    private static final Pattern STORED_PROCEDURE_PATTERN = Pattern.compile("(?i)call\\s+(\\w+)");

    public RowSource getRowSource(final NamedParameterJdbcTemplate jdbcTemplate, SessionFactory sessionFactory, RowSourceParameters rowSourceParameters) {
        return getRowSource(jdbcTemplate, sessionFactory, rowSourceParameters, null);
    }

    public RowSource getRowSource(final NamedParameterJdbcTemplate jdbcTemplate, SessionFactory sessionFactory, RowSourceParameters rowSourceParameters, OnPartsReadyListener onPartsReadyListener) {

        Class<?> clazz = rowSourceParameters.getCallerClass();

        // Делаем из пачки запросов, одну пачку частей, которые были разделены точкой с запятой
        List<String> parts = getQueryParts(rowSourceParameters.getQueries(), clazz);

        // Делаем скрипт для удаления временных таблиц (будет запущен после получения скроллера)
        final String dropTempScript = getDropTempTablesScript(parts);

        // Делаем скрипт, который для всех вызовов хранимых процедур, попытается выполнить процедуры очистки, идущие комплектом к процедурам.
        // Например, если в запросе есть "call foo();", то скрипт попытается выполнить "call foo.cleanUp();"
        final String storedProceduresCleanUpScript = getStoredProceduresCleanUpScript(parts);

        if (onPartsReadyListener != null) {
            onPartsReadyListener.onPartsReady(parts);
        }

        // Отделяем последнюю часть от остального списка. Это будет Hibernate часть
        int lastIndex = parts.size() - 1;
        String hibernateSql = parts.get(lastIndex);
        parts.remove(lastIndex);

        ScrollableResults scrollableResults = null;

        try {

            // Вытаскиваем параметры запросов из RowSourceParameters
            Map<String, Object> parameters = rowSourceParameters.getQueryParameters();

            // Выполняем JDBC часть запросов, если она есть
            if (parts.size() != 0) {
                String jdbcPart = Joiner.on(";\n\n").join(parts);
                JdbcUtils.runMultiResultStatement(jdbcTemplate, jdbcPart, parameters);
            }

            // Получаем скроллер
            scrollableResults = scroll(sessionFactory, hibernateSql, parameters, rowSourceParameters.getMappingAdder());

            return new RowSource(
                    scrollableResults,
                    rowSourceParameters.getRowProcessor(),
                    rowSource -> runCleanUpScripts(jdbcTemplate, dropTempScript, storedProceduresCleanUpScript)
            );

        } catch (Exception e) {
            tryToCleanUp(scrollableResults, jdbcTemplate, dropTempScript, storedProceduresCleanUpScript);
            throw new RowSourceServiceException(e);
        }
    }

    private void tryToCleanUp(ScrollableResults scrollableResults, NamedParameterJdbcTemplate jdbcTemplate, String dropTempScript, String storedProceduresCleanUpScript) {

        boolean error = false;

        try {
            if (scrollableResults != null) {
                scrollableResults.close();
            }
        } catch (RuntimeException e) {
            logger.error("Ошибка при обработке исключения: ", e);
            error = true;
        }

        try {
            runCleanUpScripts(jdbcTemplate, dropTempScript, storedProceduresCleanUpScript);
        } catch (RuntimeException e) {
            logger.error("Ошибка при обработке исключения: ", e);
            error = true;
        }

        if (error) {
            logger.error("dropTempScript: {}", dropTempScript);
            logger.error("storedProceduresCleanUpScript: {}", storedProceduresCleanUpScript);
        }
    }

    private void runCleanUpScripts(NamedParameterJdbcTemplate jdbcTemplate, String... scripts) {
        for (String script : scripts) {
            if (script != null) {
                JdbcUtils.runMultiResultStatement(jdbcTemplate, script);
            }
        }
    }

    private String getStoredProceduresCleanUpScript(List<String> parts) {

        StringBuilder sb = new StringBuilder();

        for (String part : parts) {

            Matcher matcher = STORED_PROCEDURE_PATTERN.matcher(part);

            while (matcher.find()) {
                String name = matcher.group(1);

                sb
                        .append("CALL callIfExists('")
                        .append(name)
                        .append("CleanUp")
                        .append("');\n");
            }
        }

        if (sb.length() == 0) {
            return null;
        } else {
            return sb.toString();
        }
    }

    private ScrollableResults scroll(SessionFactory sessionFactory, String sql, Map<String, Object> parameters, Consumer<SQLQuery> mappingAdder) {

        SQLQuery sqlQuery = sessionFactory.getCurrentSession().createSQLQuery(sql);

        // ВНИМАНИЕ!!!
        // Делаем настройки, необходимые для того, чтоб JDBC драйвер не грузил результаты запроса в один огромный лист,
        // прежде чем давать нам по нему скроллиться
        sqlQuery.setReadOnly(true);
        sqlQuery.setFetchSize(Integer.MIN_VALUE);

        // Складываем туда параметры из мапы
        for (Map.Entry<String, Object> entry : parameters.entrySet()) {
            try {
                //Хак для того, чтобы передаваемый список параметров подставлялся в запрос как надо
                if (entry.getValue() instanceof Collection) {
                    sqlQuery.setParameterList(entry.getKey(), (Collection<?>) entry.getValue());
                } else {
                    sqlQuery.setParameter(entry.getKey(), entry.getValue());
                }
            } catch (QueryParameterException ignored) {
                // Если не удалось добавить параметр к запросу, значит, такой параметр в запросе не упоминается.
                // Например, во все запросы отчетов добавляется стандартный набор параметров brand, coeff и т.д., но
                // не во всех них все эти параметры используются
            }
        }

        if (mappingAdder != null) {
            mappingAdder.accept(sqlQuery);
        }

        // ВНИМАНИЕ!!!
        // FORWARD_ONLY также необходимо для того, чтоб JDBC драйвер не грузил результаты запроса в огромный лист,
        return sqlQuery.scroll(ScrollMode.FORWARD_ONLY);
    }

    public void addMarker(List<String> parts, String marker) {

        StringBuilder partBuilder = new StringBuilder();

        ListIterator<String> it = parts.listIterator();

        while (it.hasNext()) {

            String part = it.next();

            partBuilder.setLength(0);
            partBuilder.append("/*");
            partBuilder.append(marker);
            partBuilder.append("*/");
            partBuilder.append("\n");
            partBuilder.append(part);

            it.set(partBuilder.toString());
        }
    }

    private String getDropTempTablesScript(List<String> parts) {
        StringBuilder dropTempScriptBuilder = new StringBuilder();

        for (String part : parts) {

            Matcher matcher = TEMP_TABLE_PATTERN.matcher(part);

            while (matcher.find()) {
                String temp = matcher.group(1);
                String tableName = matcher.group(2);

                dropTempScriptBuilder
                        .append("drop ")
                        .append(temp.isEmpty() ? "" : "temporary ")
                        .append("table if exists ")
                        .append(tableName)
                        .append(";\n");
            }
        }

        if (dropTempScriptBuilder.length() == 0) {
            return null;
        } else {
            return dropTempScriptBuilder.toString();
        }
    }

    private List<String> getQueryParts(List<RowSourceParameters.QueryContainer> containers, Class<?> clazz) {

        List<String> parts = new ArrayList<>(50);

        for (RowSourceParameters.QueryContainer queryContainer : containers) {

            String sql = queryContainer.getSql();
            String fileName = queryContainer.getFileName();

            if (sql == null) {
                sql = ResourceUtils.getSql(clazz, fileName);
            }

            List<String> thisQueryParts = JdbcUtils.splitMultiResultSql(sql);

            if (fileName != null) {
                addMarker(thisQueryParts, fileName);
            }

            parts.addAll(thisQueryParts);
        }

        return parts;
    }

    public interface OnPartsReadyListener {
        void onPartsReady(List<String> parts);
    }
}
