package org.taruts.dbUtils.rowSource.service;

public class RowSourceServiceException extends RuntimeException {

    public RowSourceServiceException(Throwable e) {
        super(e);
    }
}
