package org.taruts.dbUtils.rowSource;

import org.hibernate.ScrollableResults;

import java.util.Iterator;

/**
 * <br/><br/>
 * <b>Делает {@link Iterator}, на базе {@link ScrollableResults}.</b>
 * <br/><br/>
 * {@link Iterator} и {@link ScrollableResults} - похожие классы, но есть одно различие:
 * <br/><br/>
 * {@link ScrollableResults#next()} не только возвращает флаг наличия следующего элемента, но и делает продвижение вперед, то есть,
 * можно пройти всю выборку, вызвав {@link ScrollableResults#next()} нужное количество раз.
 * <br/><br/>
 * {@link Iterator#hasNext()}, в свою очередь, только проверяет наличие следующего элемента, то есть, если его вызвать
 * десять раз подряд, то продвижения вперед быть не должно, десять раз метод вернет одно и то же значение.
 * <br/><br/>
 * Аналогичным образом, но только наоборот, отличаются друг от друга {@link ScrollableResults#get()} и {@link Iterator#next()}.
 * <br/><br/>
 * {@link ScrollableResults#get()} просто возвращает значение, извлеченное методом {@link ScrollableResults#next()}, а
 * {@link Iterator#next()} как раз таки делает продвижение, и возвращает элемент, до которого продвинулись
 */
public class ScrollIterator implements Iterator<Object[]> {

    private final ScrollableResults scrollableResults;

    private boolean needShift = true;

    private boolean hasNext = false;

    public ScrollIterator(ScrollableResults scrollableResults) {
        this.scrollableResults = scrollableResults;
    }

    @Override
    public boolean hasNext() {

        if (needShift) {
            hasNext = scrollableResults.next();
            needShift = false;
        }

        return hasNext;
    }

    @Override
    public Object[] next() {

        if (needShift) {
            if (scrollableResults.next()) {
                return scrollableResults.get();
            } else {
                throw new RuntimeException("Вызов next() в отсутствие следующего элемента");
            }
        } else {

            // В следующий раз, если захотим делать Iterator#hasNext() или Iterator#next(),
            // то надо будет сделать сдвиг вперед, при помощи ScrollableResults#next()
            needShift = true;

            return scrollableResults.get();
        }
    }
}
