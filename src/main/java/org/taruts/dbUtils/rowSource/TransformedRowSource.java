package org.taruts.dbUtils.rowSource;

import com.google.common.collect.Iterators;

import java.io.Closeable;
import java.io.IOException;
import java.util.Iterator;
import java.util.function.Function;

public class TransformedRowSource<T> implements Iterable<T>, Closeable {

    private final RowSource rowSource;
    private final Function<Object[], T> javaTransformFunction;

    public TransformedRowSource(RowSource rowSource, Function<Object[], T> transformFunction) {
        this.rowSource = rowSource;
        this.javaTransformFunction = transformFunction;
    }

    public Iterator<T> iterator() {
        final Iterator<Object[]> it = rowSource.iterator();
        return Iterators.transform(it, javaTransformFunction::apply);
    }

    public void close() throws IOException {
        rowSource.close();
    }
}
