package org.taruts.dbUtils.rowSource;

public class RowSourceInterruptedException extends RuntimeException {

    public static void throwIfInterrupted() {
        if (Thread.currentThread().isInterrupted()) {
            throw new RowSourceInterruptedException();
        }
    }
}
