package org.taruts.dbUtils.rowSource;

import org.hibernate.SQLQuery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

/**
 * см. {@link RowSourceParameters#setRowProcessor(Consumer)}
 */
public class RowSourceParameters {

    private final Class<?> callerClass;

    private final List<QueryContainer> queries = new ArrayList<>();

    private final Map<String, Object> queryParameters = new HashMap<>();

    private Consumer<SQLQuery> mappingAdder;

    private Consumer<RowSource.Context> rowProcessor;

    public RowSourceParameters(Class<?> clazz) {
        callerClass = clazz;
    }

    public RowSourceParameters addSql(String sql) {

        QueryContainer queryContainer = new QueryContainer();
        queryContainer.sql = sql;

        queries.add(queryContainer);

        return this;
    }

    public RowSourceParameters addSqlFile(String fileName) {

        QueryContainer queryContainer = new QueryContainer();
        queryContainer.fileName = fileName;

        queries.add(queryContainer);

        return this;
    }

    /**
     * Устанавливает значение параметра. Установленные таким образом параметры дойдут как до нативного SQL запроса
     * Hibernate, так и для JDBC запросов, которые делают для него временные таблицы
     *
     * @param key   Имя параметра
     * @param value Значение параметра
     */
    public RowSourceParameters setParameter(String key, Object value) {
        queryParameters.put(key, value);
        return this;
    }

    /**
     * Устанавливает значение параметров. Установленные таким образом параметры дойдут как до нативного SQL запроса
     * Hibernate, так и для JDBC запросов, которые делают для него временные таблицы
     *
     * @param parameters параметры
     */
    public RowSourceParameters setParameters(Map<String, Object> parameters) {
        queryParameters.putAll(parameters);
        return this;
    }

    public Class<?> getCallerClass() {
        return callerClass;
    }

    public List<QueryContainer> getQueries() {
        return queries;
    }

    public Map<String, Object> getQueryParameters() {
        return queryParameters;
    }

    public Consumer<SQLQuery> getMappingAdder() {
        return mappingAdder;
    }

    /**
     * <p>
     * Установить метод, с добавлением маппинга к нативному SQL запросу Hibernate.
     * Данный метод будет выполнен перед {@link SQLQuery#scroll()}
     * </p>
     *
     * @param mappingAdder {@link Consumer}, в котором в {@link SQLQuery} добавляется маппинг
     */
    public RowSourceParameters setMappingAdder(Consumer<SQLQuery> mappingAdder) {
        this.mappingAdder = mappingAdder;
        return this;
    }

    public Consumer<RowSource.Context> getRowProcessor() {
        return rowProcessor;
    }

    /**
     * <p>
     * Установить метод, который будет выполняться между получением строки из запроса, и
     * внешним итератором, при помощи которого выполняется обход {@link RowSource}. Если метод будет пустой, то все
     * строки, полученные из внутреннего итератора, будут передаваться наружу, через внешний итератор, без изменений.
     * </p>
     * В методе можно изменять значения колонок текущей строки:
     * <pre>
     * {@code
     *
     * Object[] row = context.getRow();
     * row[0] = ((BigDecimal)row[0]).add((BigDecimal)row[1]);
     * }</pre>
     * Можно заменить текущую строку на другую:
     * <pre>
     * {@code
     *
     * Object[] oldRow = context.getRow();
     * Object[] newRow = new Object[1];
     * newRow[0] = ((BigDecimal)oldRow[0]).add((BigDecimal)oldRow[1]);
     * context.setRow(newRow);
     * }</pre>
     * Можно пропустить текущую строку, чтоб она не была выдана внешнему итератору:
     * <pre>
     * {@code
     *
     * context.skip();
     * }</pre>
     * Также, {@link RowSource.Context} предоставляет доступ к внутреннему итератору. Надо заметить, что внутренний
     * итератор - это не просто итератор, а итератор с буфером ({@link BufferedIterator}). Пользуясь его методами,
     * можно посмотреть строки, которые были до и строки которые будут после (насколько хватает буфера). Также можно
     * не просто заглянуть в следующую строку, а сдвинуть итератор на следующую строку, не выходя из сеанса работы с
     * текущей строкой. То есть, мы получим данные следующей строки, как-то их воспримем, но, в отличие от
     * {@link RowSource.Context#skip()}, наружу будет передана текущая строка, а <code>rowProcessor</code> в следующий
     * раз будет вызван уже для строки через следующую
     *
     * @param rowProcessor метод, в который передается {@link RowSource.Context}
     */
    public RowSourceParameters setRowProcessor(Consumer<RowSource.Context> rowProcessor) {
        this.rowProcessor = rowProcessor;
        return this;
    }

    public static class QueryContainer {
        private String sql;
        private String fileName;

        public String getSql() {
            return sql;
        }

        public String getFileName() {
            return fileName;
        }
    }
}
