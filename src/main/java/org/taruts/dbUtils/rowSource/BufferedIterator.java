package org.taruts.dbUtils.rowSource;

import com.google.common.base.Preconditions;

import java.util.Iterator;

public class BufferedIterator<T> implements Iterator<T> {

    private final Iterator<T> it;

    private final RingBuffer<T> buffer;

    public BufferedIterator(Iterator<T> it, int maxBackwardSize, int maxForwardSize) {

        this.it = it;

        buffer = new RingBuffer<>(maxBackwardSize, maxForwardSize);

        // Заполняем буфер
        for (int i = 0; i < maxForwardSize; i++) {
            if (it.hasNext()) {
                buffer.append(it.next());
            } else {
                break;
            }
        }
    }

    public BufferedIterator(Iterator<T> it, int maxSizeBackwardAndForward) {
        this(it, maxSizeBackwardAndForward, maxSizeBackwardAndForward);
    }

    @Override
    public boolean hasNext() {
        return buffer.hasNext();
    }

    @Override
    public T next() {

        Preconditions.checkState(hasNext(), "Вызов next() в отсутствие следующего элемента");

        buffer.shiftForward();

        if (it.hasNext()) {
            buffer.append(it.next());
        }

        return buffer.getCurrent();
    }

    public boolean hasNext(int i) {
        return buffer.hasNext(i);
    }

    public T peekNext(int i) {
        return buffer.getNext(i);
    }

    public boolean hasPrevious(int i) {
        return buffer.hasPrevious(i);
    }

    public T peekPrevious(int i) {
        return buffer.getPrevious(i);
    }

    public T current() {
        return buffer.getCurrent();
    }
}
