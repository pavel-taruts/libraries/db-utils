package org.taruts.dbUtils.rowSource;

import com.google.common.base.Preconditions;
import org.hibernate.ScrollableResults;

import java.io.Closeable;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;

/**
 * см. {@link RowSourceParameters#setRowProcessor(Consumer)}
 */
public class RowSource implements Iterable<Object[]>, Closeable {

    private final ScrollableResults scrollableResults;
    private final OnCloseListener onCloseListener;

    private final Consumer<Context> rowProcessor;

    private final BufferedIterator<Object[]> bufferedIterator;
    private final Iterator<Object[]> outerIterator = new Iterator<>() {

        private final Context context = new Context();

        private Iterator<Object[]> userRowsIt;

        @Override
        public boolean hasNext() {
            return userRowsIt != null && userRowsIt.hasNext() || bufferedIterator.hasNext();
        }

        @Override
        public Object[] next() {

            Preconditions.checkState(hasNext(), "Вызван метод next(), а следующего элемента нет");

            // Вполне возможно, что даже если в данном вызове найдется строка во внутреннем итераторе,
            // то rowProcessor объявит ее недействительной (вызовет Context#skip()). Типа, внешний итератор у Вас спрашивал строчку, текущую ему
            // не давайте, дайте следующую, мы ее посмотрим, может следующая сгодится.
            while (true) {

                RowSourceInterruptedException.throwIfInterrupted();

                Object[] row;

                if (userRowsIt != null) {

                    if (userRowsIt.hasNext()) {

                        // В одном из прошлых витков нам дали разобрать список строчек.
                        // Пока в данном списке не кончатся строчки, берем оттуда, а bufferedIterator ставим на паузу
                        return userRowsIt.next();

                    } else {
                        userRowsIt = null;
                    }
                }

                if (bufferedIterator.hasNext()) {
                    row = bufferedIterator.next();
                } else {
                    return null;
                }

                // У нас тут есть следующий элемент, и мы планируем его отдать внешнему потребителю, но сначала,
                // отдаем итератор в колбэк.
                //
                // В колбэке, может быть подменят текущий объект, может быть скажут, что текущая строка не айс,
                // и надо попробовать глянуть следующую... всякое может быть
                if (rowProcessor != null) {

                    context.row = row;
                    context.skip = false;
                    context.rows = null;

                    rowProcessor.accept(context);

                    if (context.skip) {
                        continue;
                    }

                    if (context.rows != null) {
                        userRowsIt = context.rows.iterator();
                        continue;
                    }

                    row = context.row;
                }

                return row;
            }
        }
    };
    private boolean iteratorGivenOut = false;

    public RowSource(ScrollableResults scrollableResults, Consumer<Context> rowProcessor, OnCloseListener onCloseListener) {

        this.scrollableResults = scrollableResults;

        this.onCloseListener = onCloseListener;

        Iterator<Object[]> scrollIterator = new ScrollIterator(scrollableResults);

        this.bufferedIterator = new BufferedIterator<>(scrollIterator, 5);

        this.rowProcessor = rowProcessor;
    }

    @Override
    public Iterator<Object[]> iterator() {

        if (iteratorGivenOut) {
            throw new RuntimeException("Из RowSource можно получить только один итератор");
        }

        iteratorGivenOut = true;

        return outerIterator;
    }

    @Override
    public void close() throws IOException {
        scrollableResults.close();
        onCloseListener.onClose(this);
    }

    @SuppressWarnings("unchecked")
    public static <T> T getValue(Object[] row, int index) {
        return (T) row[index];
    }

    @SuppressWarnings("unused")
    public static BigDecimal getBigDecimal(Object[] row, int index) {
        return (BigDecimal) row[index];
    }

    @SuppressWarnings("unused")
    public static Long getBigIntegerAsLong(Object[] row, int index) {
        BigInteger bi = getValue(row, index);
        return bi == null ? null : bi.longValue();
    }

    @SuppressWarnings("unused")
    public static Integer getBigIntegerAsInt(Object[] row, int index) {
        BigInteger bi = getValue(row, index);
        return bi == null ? null : bi.intValue();
    }

    @SuppressWarnings("unused")
    public static Boolean getBigIntegerAsBoolean(Object[] row, int index) {
        BigInteger bi = getValue(row, index);
        return bi == null ? null : bi.equals(BigInteger.ONE);
    }

    @SuppressWarnings("unused")
    public static Double getBigDecimalAsDouble(Object[] row, int index) {
        BigDecimal bi = getValue(row, index);
        return bi == null ? null : bi.doubleValue();
    }

    public interface OnCloseListener {
        void onClose(RowSource rowSource);
    }

    /**
     * см. {@link RowSourceParameters#setRowProcessor(Consumer)}
     */
    public class Context {

        private Object[] row;

        private boolean skip = false;

        private List<Object[]> rows = null;

        public BufferedIterator<Object[]> getBufferedIterator() {
            return bufferedIterator;
        }

        public void skip() {
            skip = true;
        }

        public Object[] getRow() {
            return row;
        }

        public void setRow(Object[] row) {
            this.row = row;
        }

        public void setRows(List<Object[]> rows) {
            this.rows = rows;
        }
    }
}
