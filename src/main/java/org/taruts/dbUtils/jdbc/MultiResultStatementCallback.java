package org.taruts.dbUtils.jdbc;

import org.springframework.jdbc.core.PreparedStatementCallback;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MultiResultStatementCallback<T> extends MultiResultStatementResultSetProcessor<T> implements PreparedStatementCallback<T> {

    public void onNext(int resultIndex, ResultSet rs, int rowIndex) throws SQLException {
    }

    @Override
    protected final void processResultSet(int resultIndex, ResultSet rs) throws SQLException {
        int rowIndex = 0;
        while (rs.next()) {
            onNext(resultIndex, rs, rowIndex);
        }
    }
}
