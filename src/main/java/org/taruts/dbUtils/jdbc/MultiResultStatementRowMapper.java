package org.taruts.dbUtils.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public abstract class MultiResultStatementRowMapper<T> extends MultiResultStatementCallback<List<T>> {

    private final List<T> list = new LinkedList<>();

    public abstract T mapRow(int resultIndex, ResultSet rs, int rowIndex) throws SQLException;

    @Override
    public final void onNext(int resultIndex, ResultSet rs, int rowIndex) throws SQLException {
        T t = mapRow(resultIndex, rs, rowIndex);
        list.add(t);
    }

    @Override
    public final List<T> getResult() {
        return list;
    }
}
