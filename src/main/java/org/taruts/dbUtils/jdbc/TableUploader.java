package org.taruts.dbUtils.jdbc;

import org.springframework.beans.BeanWrapperImpl;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.util.ReflectionUtils;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Modifier;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * Класс для загрузки коллекций объектов произвольного типа в таблицу в базе данных.
 * <p>
 * Если класс объектов базовый (строка, примитивы, даты и т.п.), то будет одна колонка "value". Для обычных классов
 * колонки будут созданы по именам свойств, для перечислений - по именам полей. Еще для перечислений будут автоматом
 * созданы колонки name и ordinal
 *
 * @param <T> класс, которого надо взять список объектов, создать таблицу для хранения этого списка, и загрузить туда этот список
 */
public class TableUploader<T> {

    private NamedParameterJdbcTemplate jdbcTemplate;

    private Class<T> clazz;

    private String name;

    private boolean temp;

    private Map<String, Column<T>> columns;

    private Iterable<T> objects;

    public void setJdbcTemplate(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void setClazz(Class<T> clazz) {
        this.clazz = clazz;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTemp(boolean temp) {
        this.temp = temp;
    }

    public void setObjects(Iterable<T> objects) {
        this.objects = objects;
    }

    public void execute() {
        buildColumns();
        createTable();
        fillTable();
    }

    private void addColumnsFromProperties() {

        final BeanWrapperImpl wrapper = new BeanWrapperImpl(clazz);

        PropertyDescriptor[] propertyDescriptors = wrapper.getPropertyDescriptors();

        for (PropertyDescriptor propertyDescriptor : propertyDescriptors) {

            final String propertyName = propertyDescriptor.getName();

            Class<?> propertyType = propertyDescriptor.getPropertyType();

            Function<T, ?> valueExtractor = (Function<T, Object>) object -> wrapper.getPropertyValue(propertyName);

            Column<T> column = new Column<>(propertyName, propertyType, null, valueExtractor);

            columns.put(propertyName, column);
        }
    }

    private Map<String, Column<T>> buildColumns() {

        this.columns = new LinkedHashMap<>();

        boolean simpleClass = clazz.isAssignableFrom(Date.class)
                || clazz.isAssignableFrom(Number.class)
                || clazz.isAssignableFrom(Boolean.class)
                || clazz.isAssignableFrom(CharSequence.class)
                || clazz.isAssignableFrom(Character.class);

        if (simpleClass) {

            Column<T> column = new Column<>("value", clazz, null, (Function<T, T>) input -> input);

            columns.put(column.name, column);

        } else if (clazz.isEnum()) {

            addColumnsFromFields();

            columns.get("name").indexType = Column.IndexType.PRIMARY;
            columns.get("ordinal").indexType = Column.IndexType.UNIQUE;

        } else {

            addColumnsFromProperties();
        }

        return columns;
    }

    private void addColumnsFromFields() {
        ReflectionUtils.doWithFields(clazz, field -> {

                    if (Modifier.isStatic(field.getModifiers())) {
                        return;
                    }

                    final String fieldName = field.getName();

                    Class<?> fieldType = field.getType();

                    Function<T, ?> valueExtractor = (Function<T, Object>) object -> {
                        ReflectionUtils.makeAccessible(field);
                        return ReflectionUtils.getField(field, object);
                    };

                    Column<T> column = new Column<>(fieldName, fieldType, null, valueExtractor);

                    columns.put(fieldName, column);
                }
        );
    }

    private void createTable() {

        StringBuilder sb = new StringBuilder();

        sb.append("drop ");
        if (temp) {
            sb.append("temporary ");
        }
        sb
                .append("table if exists ")
                .append(name)
                .append(";");

        sb.append("create ");
        if (temp) {
            sb.append("temporary ");
        }
        sb
                .append("table ")
                .append(name)
                .append(" (");

        boolean first = true;

        for (Column<T> column : columns.values()) {

            if (column.sqlType == null) {
                throw new IllegalArgumentException("Для колонки " + column.name + " не указан SQL тип. " +
                        "Наверное потому, что для соответствующего JavaClass " +
                        "не сделали автоматическое вычисление SQL типа");
            }

            if (first) {
                first = false;
            } else {
                sb.append(',');
            }

            sb
                    .append(column.name)
                    .append(' ')
                    .append(column.sqlType);

            Column.IndexType indexType = column.indexType;

            if (indexType != null) {
                sb
                        .append(' ')
                        .append(indexType);
            }
        }

        sb.append(")");

        if (temp) {
            sb.append(" ENGINE = memory");
        } else {
            // Не временные таблицы тоже можно было бы сделать memory (они же заполяются каждый раз при старте системы)
            // Но тут, скорее всего, все осложнилось бы репликацией
            sb.append(" ENGINE = InnoDB");
        }

        JdbcUtils.runMultiResultStatement(jdbcTemplate, sb.toString());
    }

    private void fillTable() {

        BatchInserter inserter = new BatchInserter(jdbcTemplate, name);

        for (T object : objects) {

            Map<String, Object> rowValues = new HashMap<>();

            for (Column<T> column : columns.values()) {
                rowValues.put(
                        column.name,
                        column.getValue(object)
                );
            }

            inserter.addRow(rowValues);
        }

        inserter.flush();
    }

    public static class Column<ObjectType> {

        private final String name;
        private final java.util.function.Function<ObjectType, ?> valueExtractor;
        private final String sqlType;
        private IndexType indexType;

        public Column(String name, Class<?> javaType, IndexType indexType, Function<ObjectType, ?> valueExtractor) {
            this.name = name;
            this.indexType = indexType;
            this.valueExtractor = valueExtractor;

            if (javaType.equals(String.class)) {
                this.sqlType = "varchar(255)";
            } else if (javaType.equals(Integer.class) || javaType.equals(Integer.TYPE)) {
                this.sqlType = "int";
            } else if (javaType.equals(Boolean.class) || javaType.equals(Boolean.TYPE)) {
                this.sqlType = "bit(1)";
            } else {
                this.sqlType = null;
            }
        }

        public Object getValue(ObjectType object) {
            return valueExtractor.apply(object);
        }

        public enum IndexType {
            PRIMARY {
                @Override
                public String toString() {
                    return "PRIMARY KEY";
                }
            },
            UNIQUE
        }
    }
}
