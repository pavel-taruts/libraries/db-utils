package org.taruts.dbUtils.jdbc;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCallback;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MultiResultStatementResultSetProcessor<T> implements PreparedStatementCallback<T> {

    public T getResult() {
        return null;
    }

    final public T doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
        int resultIndex = -1;
        boolean isSelect = ps.execute();
        while (true) {
            resultIndex++;
            if (isSelect) {
                ResultSet rs = ps.getResultSet();
                processResultSet(resultIndex, rs);
            } else {
                int updateCount = ps.getUpdateCount();
                if (updateCount == -1) {
                    break;
                }
            }
            isSelect = ps.getMoreResults(Statement.CLOSE_CURRENT_RESULT);
        }
        return getResult();
    }

    protected void processResultSet(int resultIndex, ResultSet rs) throws SQLException {
    }
}
