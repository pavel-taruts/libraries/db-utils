package org.taruts.dbUtils.jdbc;

import com.google.common.base.Preconditions;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.taruts.dbUtils.generalPurpose.MapUtils;

import java.io.Flushable;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Map;

@SuppressWarnings("SqlDialectInspection")
public class BatchInserter implements Flushable {

    private final String tableName;
    private final StringBuilder sb;
    private final String[] names;
    private final int initialLength;
    private final NamedParameterJdbcTemplate destinationJdbcTemplate;
    private final ValueProvider valueProvider;
    private final SimpleDateFormat sqlDateTimeFormat = JdbcUtils.getSqlDateFormat();
    private boolean isEmpty;
    private int megabytesInPortion = 10;

    @SuppressWarnings("unused")
    public BatchInserter(NamedParameterJdbcTemplate destinationJdbcTemplate, final String tableName, ResultSet rs) {
        this(destinationJdbcTemplate, tableName, null, new ResultSetValueProvider(rs));
    }

    @SuppressWarnings("unused")
    public BatchInserter(NamedParameterJdbcTemplate destinationJdbcTemplate, final String tableName, String[] names, ResultSet rs) {
        this(destinationJdbcTemplate, tableName, names, new ResultSetValueProvider(rs));
    }

    public BatchInserter(NamedParameterJdbcTemplate destinationJdbcTemplate, final String tableName) {
        this(destinationJdbcTemplate, tableName, null, (ValueProvider) null);
    }

    public BatchInserter(NamedParameterJdbcTemplate destinationJdbcTemplate, String tableName, String[] names) {
        this(destinationJdbcTemplate, tableName, names, (ValueProvider) null);
    }

    public BatchInserter(NamedParameterJdbcTemplate destinationJdbcTemplate, final String tableName, String[] names, ValueProvider valueProvider) {

        this.destinationJdbcTemplate = destinationJdbcTemplate;
        this.valueProvider = valueProvider;
        this.tableName = tableName;

        if (names == null) {
            this.names = queryNames();
        } else {
            this.names = names;
        }

        sb = new StringBuilder("insert into ");

        sb.append(tableName);

        sb.append("(");
        boolean firstColumn = true;
        for (String name : this.names) {
            if (!firstColumn) {
                sb.append(",");
            }
            sb.append(name);
            firstColumn = false;
        }
        sb.append(")\nvalues\n");

        initialLength = sb.length();

        isEmpty = true;
    }

    public String[] queryNames() {
        return destinationJdbcTemplate.execute("select * from " + tableName + " where false", Collections.emptyMap(), ps -> {

            try (ResultSet rs = ps.executeQuery()) {
                ResultSetMetaData metaData = rs.getMetaData();

                int count = metaData.getColumnCount();

                String[] names = new String[count];

                for (int i = 1; i <= count; i++) {
                    String name = metaData.getColumnName(i);
                    names[i - 1] = name;
                }

                return names;
            }
        });
    }

    public void flush() {
        if (!isEmpty) {
            JdbcUtils.runMultiResultStatement(destinationJdbcTemplate, sb.toString());
            sb.setLength(initialLength);
            isEmpty = true;
        }
    }

    @SuppressWarnings("unused")
    public void addRow() {
        Preconditions.checkNotNull(valueProvider, "Value provider must be set");
        addRow(valueProvider);
    }

    public void addRow(Map<? super String, ?> values) {
        addRow(new MapValueProvider(values));
    }

    public void addRow(Object... mapParts) {
        addRow(MapUtils.asPropertyMap(mapParts));
    }

    public void addRow(ResultSet rs) {
        addRow(new ResultSetValueProvider(rs));
    }

    public void addRow(ValueProvider valueProvider) {

        if (sb.length() >= megabytesInPortion * 1024 * 1024 - 500) {
            flush();
        }

        if (!isEmpty) {
            sb.append(",\n");
        }

        sb.append("(");
        boolean firstColumn = true;
        for (String name : names) {

            Object val = valueProvider.get(name);

            if (!firstColumn) {
                sb.append(",");
            }
            sb.append(JdbcUtils.sqlValue(val, sqlDateTimeFormat));
            firstColumn = false;
        }
        sb.append(")");

        isEmpty = false;
    }

    public void setMegabytesInPortion(int megabytesInPortion) {
        this.megabytesInPortion = megabytesInPortion;
    }

    public abstract static class ValueProvider {
        abstract protected Object get(String name);
    }

    public static class ResultSetValueProvider extends ValueProvider {

        ResultSet rs;

        public ResultSetValueProvider(ResultSet rs) {
            this.rs = rs;
        }

        protected Object get(String name) {
            try {
                return rs.getObject(name);
            } catch (SQLException e) {
                throw new RuntimeException();
            }
        }
    }

    public static class MapValueProvider extends ValueProvider {

        Map<? super String, ?> values;

        public MapValueProvider(Map<? super String, ?> values) {
            this.values = values;
        }

        protected Object get(String name) {
            return values.get(name);
        }
    }
}
