package org.taruts.dbUtils.jdbc;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class JdbcUtils {

    public static void runMultiResultStatement(NamedParameterJdbcTemplate jdbcTemplate, String sql) {
        jdbcTemplate.execute(sql, Collections.emptyMap(), new MultiResultStatementCallback<>());
    }

    public static void runMultiResultStatement(NamedParameterJdbcTemplate jdbcTemplate, String sql, Map<String, Object> paramMap) {
        jdbcTemplate.execute(sql, paramMap, new MultiResultStatementCallback<>());
    }

    @SuppressWarnings("unused")
    public static void downloadTable(NamedParameterJdbcTemplate sourceJdbcTemplate, String sql, NamedParameterJdbcTemplate destinationJdbcTemplate, String tableName, final String... names) {
        downloadTable(sourceJdbcTemplate, sql, Collections.emptyMap(), destinationJdbcTemplate, tableName, names);
    }

    public static void downloadTable(NamedParameterJdbcTemplate sourceJdbcTemplate, String sql, Map<String, Object> paramMap, final NamedParameterJdbcTemplate destinationJdbcTemplate, String tableName, final String... names) {

        final BatchInserter batchInserter = new BatchInserter(destinationJdbcTemplate, tableName, names);

        batchInserter.setMegabytesInPortion(10);

        sourceJdbcTemplate.execute(sql, paramMap, new MultiResultStatementCallback<Integer>() {

            @Override
            public void onNext(int resultIndex, ResultSet rs, int rowIndex) throws SQLException {
                batchInserter.addRow(rs);
            }
        });

        batchInserter.flush();
    }

    public static String sqlValue(Object val, SimpleDateFormat sqlDateTimeFormat) {

        if (val == null) {
            return "null";
        }

        if (val.getClass() == String.class) {
            return "'" + StringEscapeUtils.escapeSql((String) val) + "'";
        }

        if (val instanceof Date) {
            return "'" + sqlDateTimeFormat.format((Date) val) + "'";
        }

        return String.valueOf(val);
    }

    public static SimpleDateFormat getSqlDateFormat() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    }

    public static List<String> splitMultiResultSql(String sql) {

        //@formatter:off
        final byte NEUTRAL        = 0;
        final byte INLINE_COMMENT = 1;
        final byte LINE_COMMENT   = 2;
        final byte STRING         = 3;
        //@formatter:on

        byte state = NEUTRAL;

        List<String> parts = new LinkedList<>();

        StringBuilder sb = new StringBuilder();

        char prev1 = (char) 0;
        char prev2 = (char) 0;

        for (int i = 0; i < sql.length(); i++) {

            char c = sql.charAt(i);

            if (sb.length() == 0 && Character.isWhitespace(c)) {
                continue;
            }

            if (state == NEUTRAL && c == ';') {

                parts.add(sb.toString());
                sb.setLength(0);
                prev1 = (char) 0;
                prev2 = (char) 0;
                continue;

            } else if (state == NEUTRAL && prev1 == '/' && c == '*') {
                state = INLINE_COMMENT;
            } else if (state == INLINE_COMMENT && prev1 == '*' && c == '/') {
                state = NEUTRAL;
            } else if (state == NEUTRAL && prev2 == '-' && prev1 == '-' && (c == ' ' || c == '\t')) {
                state = LINE_COMMENT;
            } else if (state == NEUTRAL && c == '#') {
                state = LINE_COMMENT;
            } else if (state == LINE_COMMENT && (c == '\n' || c == '\r')) {
                state = NEUTRAL;
            } else if (state == NEUTRAL && c == '\'') {
                state = STRING;
            } else if (state == STRING && prev1 != '\\' && c == '\'') {
                state = NEUTRAL;
            }

            prev2 = prev1;
            prev1 = c;

            sb.append(c);
        }

        if (sb.length() > 0) {
            parts.add(sb.toString());
        }

        parts.removeIf(StringUtils::isBlank);

        return parts;
    }

    /**
     * Создает постоянную таблицу {@code tableName} с использованием {@link NamedParameterJdbcTemplate},
     * указанного в параметр {@code jdbcTemplate}.
     * Структура таблицы создается автоматически для хранения объектов класса {@code T}.
     *
     * @param jdbcTemplate {@link NamedParameterJdbcTemplate}, через который нужно создавать таблицу
     * @param tableName    Имя таблицы, которую надо создать, и загрузить туда данные
     * @param objects      Список объектов, которые надо загрузить в таблицу в качестве строк
     * @param clazz        Класс загружаемых объектов. Технический параметр, нужен для того чтоб там внутри хорошо работали generics
     * @param <T>          Класс загружаемых объектов
     */
    public static <T> void uploadTable(NamedParameterJdbcTemplate jdbcTemplate, String tableName, Iterable<T> objects, Class<T> clazz) {

        TableUploader<T> table = new TableUploader<>();
        table.setJdbcTemplate(jdbcTemplate);
        table.setName(tableName);
        table.setTemp(false);
        table.setClazz(clazz);
        table.setObjects(objects);

        table.execute();
    }

    /**
     * Создает временную таблицу {@code tableName} с использованием {@link NamedParameterJdbcTemplate},
     * указанного в параметр {@code jdbcTemplate}.
     * Структура таблицы создается автоматически для хранения объектов класса {@code T}.
     *
     * @param jdbcTemplate {@link NamedParameterJdbcTemplate}, через который нужно создавать таблицу
     * @param tableName    Имя таблицы, которую надо создать, и загрузить туда данные
     * @param objects      Список объектов, которые надо загрузить в таблицу в качестве строк
     * @param clazz        Класс загружаемых объектов. Технический параметр, нужен для того чтоб там внутри хорошо работали generics
     * @param <T>          Класс загружаемых объектов
     */
    public static <T> void uploadTempTable(NamedParameterJdbcTemplate jdbcTemplate, String tableName, Iterable<T> objects, Class<T> clazz) {

        TableUploader<T> table = new TableUploader<>();
        table.setJdbcTemplate(jdbcTemplate);
        table.setName(tableName);
        table.setTemp(true);
        table.setClazz(clazz);
        table.setObjects(objects);

        table.execute();
    }
}
